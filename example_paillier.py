from phe import paillier as pl


public_key, private_key = pl.generate_paillier_keypair(n_length=256)

#The returned result from generate_paillier_keypair are PaillierPublicKey and PaillierPrivateKey object
print('Public and Private Keys')
print('-----------------------------')

print('Type of public_key is ', type(public_key))
print('Type of private_key is ', type(private_key))

#you can encrypt the numerical data using encrypt() function in public_key object
print('\nEncrypting numerical data')
print('-----------------------------')
encrypted_24 = public_key.encrypt(24)

#The encrypted result is an EncryptedNumber object
print('Type of encrypted_data is ', type(encrypted_24))

#you can see the ciphertext (encrypted data) by calling ciphertext() function the EncryptedNumber object
print('Ciphertext of 24 is ', encrypted_24.ciphertext())

print('\nArithmetic operations on encrypted data')
print('-----------------------------')
#you can perform basic summation between EncryptedNumber with non-encrypted data 
encrypted_data_plus_6 = encrypted_24+6
encrypted_data_minus_4 = encrypted_24-4

#summation between encrypted data is also OK
encrypted_6 = public_key.encrypt(6)
encrypted_plus_encrypted = encrypted_24 + encrypted_6

#For multiplication, you can only perform this operation between encrypted data and plaintext
encrypted_data_mult_2 = encrypted_24*2
encrypted_data_div_2 = encrypted_24/2


#Similarly, you can see the ciphertext of the result using ciphertext function
print('Ciphertext of encrypted_24 + 6 is ', encrypted_data_plus_6.ciphertext())

#you can decrypt the result using decrypt() function in private key object
print('Result of encrypted_24 + 6 is ', private_key.decrypt(encrypted_data_plus_6))
