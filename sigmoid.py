from Pyfhel import Pyfhel, PyPtxt, PyCtxt
from logistic_regression_with_fhe import encrypt_sigmoid_coeff, get_approx_sigmoid, sigmoid_for_ciphertext
import random
import numpy as np


def apply_approx_sigmoid_to_plaintext(approximated_sigmoid_coeff, x):
    '''
    Function to approximate sigmoid function based on Taylor series coefficients.

    Args:
        approximated_sigmoid_coeff: coefficients in Taylor series.
        x: input value

    Returns:
        approximated sigmoid value.

    '''
    power = len(approximated_sigmoid_coeff)
    approx = 0
    approx += 0.5
    for i in range(len(approximated_sigmoid_coeff)):
        approx += approximated_sigmoid_coeff[i]*pow(x,power-i)
    return approx

    
def sigmoid_exact(x):
    '''
    Real sigmoid function

    Args: 
        x: numpy array
    
    Returns:
        numpy array that stores outputs from sigmoid function on each element within x.
    '''
    return 1 / (1 + np.exp(-x))


def validation_of_approx_sigmoid():
    '''
    A function for comparing real sigmoid values and approximated ones.
    '''
    x = np.linspace(-3,3,20)  # default setting to be approxed within [-3,3]
    he = Pyfhel()           # Creating empty Pyfhel object
    he.contextGen(p=65537, m=8192*2, flagBatching=True, fracDigits=8)
    he.keyGen()             # Key Generation.
    encrypted_coeff = encrypt_sigmoid_coeff(he,get_approx_sigmoid())
    for_print = [he.decryptFrac(encrypted_coeff[i]) for i in range(len(encrypted_coeff))]
    print("sigmoid is approximated as => ", for_print)
    for_ptxt = get_approx_sigmoid()
    
    print('{0:<25}|{1:<30}|{2:<30}|{3:<30}'.format('X', 'Exact sigmoid(x)', 'Approximated sigmoid(x)', 'Approximated sigmoid(x) on ctxt'))
    print('-'*120)
    for i in range(len(x)):
        # exact sigmoid with plain text
        y = sigmoid_exact(x[i])
        # approxed sigmoid with plain text
        y_ = apply_approx_sigmoid_to_plaintext(for_ptxt,x[i])

        # using approx_sigmoid with ciphertext: input = element-wise-ctxt, ouput: sigmoid-applied-ctxt
        encx = he.encryptFrac(x[i])

        siged_x = sigmoid_for_ciphertext(he,encrypted_coeff,encx)

        print('{0:<25}|{1:<30}|{2:<30}|{3:<30}'.format(x[i], y, y_, he.decryptFrac(siged_x)))


def main():
    validation_of_approx_sigmoid()

if __name__ == "__main__":
    main()
        
        
