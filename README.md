# pyenv pythonの仮想環境を作る
（ネットワーク状況にもよりますが、５分ほどかかります。）
```
sudo apt-get update &&\
git clone https://github.com/pyenv/pyenv.git ~/.pyenv &&\
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile &&\
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile &&\
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n eval "$(pyenv init -)"\nfi' >> ~/.profile &&\
source ~/.profile &&\
sudo apt install -y gcc make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget 
curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev &&\
pyenv install 3.6.3 &&\
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv &&\
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.profile &&\
source ~/.profile &&\
pyenv virtualenv 3.6.3 myenv 

```

この後に、
```
source ~/.pyenv/versions/myenv/bin/activate
```
で作った仮想環境"myenv"に入ります。この後は必要なライブラリをpipインストールしてください。


# Paillier
## 環境構築

pipでPaillierをインストールします

```
pip3 install phe
```

## Play around with Paillier

まず始めに、pheライブラリをimportします。

```
from phe import paillier as pl
```
Paillierを使って数値データを暗号化/復号化するには、公開鍵と秘密鍵を生成する必要があります。


```
public_key, private_key = pl.generate_paillier_keypair(n_length=256)
```

**public_key**オブジェクトの **encrypt()** を使用して、任意の数値データを暗号化できます。

```
encrypted_24 = public_key.encrypt(24)
```

暗号化されたデータは**EncryptedNumber**オブジェクトです。

Paillierでは、暗号化されたデータと平文の間で加算操作を実行できます。
```
encrypted_data_plus_6 = encrypted_24+6
```

結果(**encrypted_data_plus_6**)はまだ**EncryptedNumber**オブジェクトです

以下のように、**ciphertext()** を使用すると、暗号化されたデータの暗号文を確認できます。

```
print('Ciphertext of encrypted_24 + 6 is ', encrypted_data_plus_6.ciphertext())
```
暗号化されたデータ間の加算操作も実行できます。

```
encrypted_6 = public_key.encrypt(6)
encrypted_plus_encrypted = encrypted_24 + encrypted_6
```
**乗算**と**除算**に関しては、暗号化されたデータと平文の間でしかできません。


```
encrypted_data_mult_2 = encrypted_24*2
encrypted_data_div_2 = encrypted_24/2
```

暗号化されたデータを復号化するには、**private_key** オブジェクトの **decrypt()** を使用してください。


```
print('Result of encrypted_24 + 6 is ', private_key.decrypt(encrypted_data_plus_6))
```


# Fully Homomorphic Encryption
Fully Homomorphic Encryption (FHE) は **暗号文(暗号化データ)のまま** で加算、減算、乗算などを実行できる暗号方式の一つです。

HELibやMicrosoftのSEALなどの一般的なFHEライブラリのほとんどは、すべてC++で実装されています。ただし、簡単にするために、ここではSEALのPythonラッパーを使用します。


## 環境構築

pipでpyfhelをインストールします。
   ```
   pip3 install pyfhel
   ```

## Play around with FHE
まず始めに、Pyfhel, PyCtxt, PyPtxtをimportします。
```
from Pyfhel import Pyfhel, PyCtxt, PyPtxt
```

- PyPtxtはプレーンテキストデータを格納するためのオブジェクトです。
- PyCtxtは暗号文を格納するのためのオブジェクトです。
- Pyfhelには、PyCtxtに対する暗号化、復号化、および算術演算に必要なすべての関数が含まれています。

次に **contextGen()** を使ってコンテキスト環境を生成する必要があります。FHE暗号化方式の設定の一種です。

```
he = Pyfhel()           # 空のPyfhelオブジェクトを生成
he.contextGen(p=65537, m=4096)  #コンテキストを初期化
```
コンテキスト初期化後、鍵を生成できます。

```
he.keyGen()             # 鍵を生成
```

**Pyfhel**オブジェクトの**saveContext()**、**savepublicKey()**、および **savesecretKey()** を使用すると、コンテキスト、公開鍵、および秘密鍵をエクスポートできます。


```
he.saveContext('context.txt') #コンテキストをファイルに保存
he.savepublicKey('pub.key')  #公開鍵をファイルに保存
he.savesecretKey('secret.key') #秘密鍵をファイルに保存
```

int型のデータを暗号化および復号化するには、Pyfhelの**encryptInt()** and **decryptInt()** を使います。


```
enc_5 = he.encryptInt(5)
output = he.decryptInt(enc_5)   ## outputは5になります
```
PyCtxtオブジェクトの暗号文をファイルに保存するには、**save()** を使います。

```
enc_5.save('ctxt_enc_5.txt')
```

float型のデータを暗号化および復号化するには、**encryptFrac()** and **decryptFrac()** を使います。

```
enc_2p5 = he.encryptFrac(2.5)
output = he.decryptFrac(enc_2p5)   ## outputは2.5になります
```
array型のデータを暗号化および復号化するには、**encryptBatch()** and **decryptBatch()** を使います。ただし、配列の各要素は整数データである必要があります。
また、コンテキストを初期化する際に、**flagBatching=True**にする必要があります。
```
he = Pyfhel()           # 空のPyfhelオブジェクトを生成
he.contextGen(p=65537, m=4096, flagBatching=True)  #コンテキストを初期化
```

```
enc_array = he.encryptBatch(np.array([1,2,3]))
output = he.decryptBatch(enc_array) #　outputは[1 2 3 ............ ]
```

Pythonでは、インデックスを使用して復号化された配列から元のデータを取得できます。

```
print(output[:3])
```

+、-、および*を使用して、暗号文間で加算、減算、乗算を実行できます。

```
encA = he.encryptInt(2)
encB = he.encryptInt(3)

enc_A_plus_B = encA + encB
enc_A_minus_B = encA - encB
enc_A_multiply_B = encA * encB
```
ただし、同じ暗号化方式で暗号化された暗号文に対して操作を行うことはできます。したがって、次のコードはexceptionをスローします。


```
encA = he.encryptInt(2)
encB = he.encryptFrac(5.0)

tmp = encA + encB  #encAとencBのエンコード方法が異なるため、このコード行はexceptionをスローします。
```
暗号文のノイズレベルを常にチェックすることが非常に重要です。

**PyFhel**オブジェクトの **noiseLevel()** を使用してノイズレベルをチェックできます。


```
encA = he.encryptInt(2)

he.noiseLevel(encA) # encAのノイズレベルを返します
```

**暗号文に対して基本的な算術演算を実行するたびに、ノイズレベルが低下します。
ノイズレベルが0になると、元のデータを取得できなくなります。**



# For your future references ...
このリポジトリでは、Paillierとfheを使用して、以下のPythonプログラムを実装しました。

1. Linear regression using paillier (linear_regression_with_paillier.py)  
2. Logistic regression using paillier (linear_regression_with_paillier.py)
3. Linear regression using paillier (linear_regression_with_fhe.py)  
4. Logistic regression using paillier (linear_regression_with_fhe.py)

これらのコードを実行する際は、**scikit-learn** をインストールしてください


```
pip3 install scikit-learn
```

**Note:** **scikit-learn**をインストールすると、 **numpy** も自動的にインストールされます。
