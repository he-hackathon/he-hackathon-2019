from Pyfhel import Pyfhel, PyPtxt, PyCtxt

from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
import random
import numpy as np


def make_datasets_for_linear_regression(total_samples=10, train_ratio=0.8, coeff=2, bias=1):
    '''
    This function generates two very simple dataset (training and testing) for linear regression.

    Args:
        total_samples: number of samples to be generated. Default value is 100.
        train_ratio: ratio for training portion. The remaining portion is used for testing. 
            The range of train_ratio should be 0-1.0. Its default value is 0.8.
        coeff: coefficient in linear function. Default value is 2.
        bias: bias in linear function. Default value is 1.
         
    Returns:
        Two lists of numeric values. 
        The first one is the training dataset and the second one is the testing dataset.
    '''
    train_size = int(total_samples*train_ratio)
    dataset = [(x, 2*x+bias) for x in range(total_samples)]
    return dataset[:train_size], dataset[train_size:]


def encrypt_linear_regression_dataset(he:Pyfhel, dataset:list):
    '''
    This function encrypts a linear regression dataset using FHE.

    Args:
        he: Pyfhel object that has been initialized with public keys and contexts.
        input_data: A list that stores linear regression dataset. 

    Returns:
        a list of encrypted data.
    '''
    enc_input = [(he.encryptFrac(x),he.encryptFrac(y)) for x, y in dataset]
    return enc_input


def update_weights(he:Pyfhel, encrypted_training_dataset:list, weight:PyCtxt, bias:PyCtxt, encrypted_learning_rate:PyCtxt, encrypted_dataset_size:PyCtxt):
    '''
    This function is part of the training process of linear regression. 
    It is used for updating weight and bias in each epoch.

    Args:
        he: Pyfhel object that has been initialized with public key and contexts.
        encrypted_training_dataset: a python list that stores encrypted linear-regression dataset.
        weight: PyCtxt of weight.
        bias: PyCtxt of bias.
        encrypted_learning_rate: PyCtxt of learning rate.
        encrypted_dataset_size: PyCtxt of size of training dataset.

    Returns:
        Updated PyCtxts of weights and bias.
    '''
    error_b = he.encryptFrac(0.0)
    error_w = he.encryptFrac(0.0)

    #calculate result
    for x,y in encrypted_training_dataset:
        prediction = weight*x + bias
        error = prediction - y
        #for derivatives
        error_b = error_b + error
        error_w = error_w + error*x


    tmp1 = encrypted_learning_rate*error_b*encrypted_dataset_size
    tmp2 = encrypted_learning_rate*error_w*encrypted_dataset_size
    
    bias = bias + he.negate(tmp1)
    weight = weight + he.negate(tmp2)

    print('(plain text space) Weight and bias >',he.decryptFrac(weight), ',', he.decryptFrac(bias))
    return weight, bias

def user_decrypt(he:Pyfhel, weight:PyCtxt, bias:PyCtxt):
    '''
    This function is used for removing noise of PyCtxt of weight and bias. 
    Basically, this function decrypts those PyCtxt and re-encrypts the original values again.

    Args:
        weight: PyCtxt of weight.
        bias: PyCtxt of bias. 

    Returns:
        weight: PyCtxt of weight without noise.
        bias: PyCtxt of bias without noise. 
    '''
    return he.encryptFrac(he.decryptFrac(weight)), he.encryptFrac(he.decryptFrac(bias))

def training_linear_regression(he:Pyfhel, encrypted_training_dataset:list,epoch=10, learning_rate=0.05):
    '''
    Training function for linear regression algorithm.

    Args:
        he: Pyfhel object that has been initialized with keys and contexts.
        encrypted_training_dataset: a python list that stores encrypted linear-regression dataset.
        epoch: number of epoch for training. Default value is 10.
        learning_rate: learning rate. Default value is 0.05.

    Returns:
        weight: PyCtxt of weight.
        bias: PyCtxt of bias. 
    '''
    #initialize weight and bias
    weight =  he.encryptFrac(0.0)
    bias = he.encryptFrac(0.0)
    
    learning_rate = he.encryptFrac(learning_rate)
    dataset_size = he.encryptFrac(1./len(encrypted_training_dataset))
    for i in range(epoch):
        weight, bias = update_weights(he, encrypted_training_dataset, weight, bias, learning_rate, dataset_size)
        weight, bias = user_decrypt(he, weight, bias)
        
    return weight, bias


def testing_linear_regression(trained_weight:PyCtxt, trained_bias:PyCtxt, encrypted_testing_dataset:list):
    '''
    This function calculates the prediction value from trained (and encrypted) weight and bias.

    Args:
        trained_weight: PyCtxt of trained_weight.
        trained_bias: PyCtxt of trained_bias.
        encrypted_testing_dataset: a python list that stores encrypted linear-regression dataset.

    Returns:
        list of PyCtxt, i.e., encrypted prediction output.
    '''
    output = []
    for x,y in encrypted_testing_dataset:
        prediction = trained_weight*x + trained_bias
        output.append(prediction)

    return output

def decrypt_prediction_results(he:Pyfhel, prediction_results:list):
    '''
    This function is used for decrypting the output made by testing_linear_regression function.

    Args:
        he: Pyfhel object that has been initialized with private key and contexts.
        prediction_results: a list of PyCtxt generated by testing_linear_regression function.

    Returns:
        a list of prediction values.
    '''
    return [he.decryptFrac(x) for x in prediction_results]

def linear_regression():
    '''
    This function shows example of training and running linear regression implemented by using FHE scheme.
    '''
    #number of samples in the dataset
    data_size = 10

    #make sample dataset  (2x+1)
    training_dataset, testing_dataset = make_datasets_for_linear_regression(total_samples=data_size, train_ratio=0.8)

    #training parameters
    epoch = 10
    learning_rate = 0.05
    
    #### KEY GENERATION ####
    
    he = Pyfhel()           # Creating empty Pyfhel object
    he.contextGen(p=65537, m=8192*2, flagBatching=True, fracDigits=8)
    he.keyGen()             # Key Generation.

    he.saveContext('context.txt') #save context to a file
    he.savepublicKey('pub.key')  #save public key to a file
    he.savesecretKey('secret.key') #save private key to a file

    print('============== Encrypt training & testing dataset ===================')
    #encrypt x & y
    enc_train = encrypt_linear_regression_dataset(he, training_dataset)
    enc_test = encrypt_linear_regression_dataset(he, testing_dataset)


    #train model using plain text
    print('==============Training with raw (plain text) data===================')
    weight, bias = training_linear_regression(he, enc_train, epoch=epoch, learning_rate=learning_rate)
    
    print('------------------')
    print(he.decryptFrac(weight))
    print(he.decryptFrac(bias))
    
    print('============== Prediction on encrypted data ===================')
    #prediction over encrypted data
    prediction_results = testing_linear_regression(weight, bias, enc_test)

    #decrypt prediction results
    raw_results = decrypt_prediction_results(he, prediction_results)
    for x in range(len(raw_results)):
        print('predicted=', raw_results[x], '\tactual=', testing_dataset[x][1])
    

if __name__ == '__main__':
    linear_regression()
    
