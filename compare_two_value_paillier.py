from phe import paillier as pl

def binarize_int(a:int, binary_length=5):
    '''
    Make Integer Into Binaryzed Representaion, 
    For Example, 3 => 00011,  8 => 01000
    Note that Input Cannot Exceed 2^(binary_length)

    Args:
        a: integer
        binary_length: length of output binary string

    Returns:
        binary string of a which its length is equal to binary_length
    '''
   
    bit_a = bin(a)
    b_ind = bit_a.rfind("b")
    bit_a = bit_a[b_ind+1:]
    diff = binary_length - len(bit_a)
    for i in range(diff):
        bit_a = "0" + bit_a

    return bit_a

def encrypt_binarized_int(bit_a:str, pk):
    '''
    Encrypt Each Binary with Paillier
    For Example, 3 => 00011 => ['89729857','2398739572','3724983270','223848920','09873522']

    Args:
        bit_a: input binary string
        pk: PaillierPublicKey object. It is used for encryption

    Returns:
        A python list of all encrypted binary from bit_a.
    '''
    enc_bit_a = []
    for i in range(len(bit_a)):
        enc_bit_a.append(pk.encrypt(int(bit_a[i])))
    return enc_bit_a

def compare_encrypted_binarized_input_with_plaintext(a:list, b:str, pk):
    '''
    Compare One Encrypted Int with Plaintext Int

    Args: 
        a: list of encrypted binary string (list of EncryptedNumber objects).
        b: binary string for comparison


    Returns:
        A python list, which Includes Encrypted Judging Number, If Any Of Them is 0, It Indicates a < b.
    '''
    
    result = []
    for i in range(len(a)):
        second_half = pk.encrypt(0)
        for j in range(0,i):
            tmp = int(b[j])
            # print(tmp)
            if tmp == 0:
                xor_result = a[j]
                second_half = second_half + xor_result
            elif tmp == 1:
                xor_result = pk.encrypt(1) - a[j]
                second_half = second_half + xor_result
            else:
                raise Exception("b should be binary representation")
        second_half = second_half * 3
        tmp_result = a[i] - pk.encrypt(int(b[i])) + pk.encrypt(1) + second_half
        result.append(tmp_result)
    
    return result


if __name__ == "__main__":
    # This Program is Sample Code for Compare Encrypted Number with Plaintext.
    # Usually This is Not Allowed for CipherText, Because If It is, By Compare Numerous Encrypted Number,
    # One can Find at Least Sort CipherText in Order, which Reveal Information of Secret Data.

    # Compare int a and int b.
    # This Program can Compare One Encrypted Number, in This Case, a, 
    # With Plaintext, in This Case, b.
    a = 4
    b = 9

    pk, sk = pl.generate_paillier_keypair(n_length=256)

    # Make Two Number Into Binary Representation
    bit_a = binarize_int(a)
    bit_b = binarize_int(b)

    # Encrypt Binary Representation of a
    enc_bit_a = encrypt_binarized_int(bit_a, pk)

    # Compare Encrypted int a and Plaintext b
    result = compare_encrypted_binarized_input_with_plaintext(enc_bit_a, bit_b, pk)

    # If Decrypted Result Includes 0, a < b
    # Otherwise, a >= b
    decrypt_result = [sk.decrypt(result[i]) for i in range(len(result))]

    # decrypt_result = [1, 0, 5, 7, 6], which Includes 0, So, a < b.
    print('decrypt result is ', decrypt_result)
    print('a > b or not: ', not any([x == 0 for x in decrypt_result]))

