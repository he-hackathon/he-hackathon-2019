from Pyfhel import Pyfhel, PyCtxt, PyCtxt

he = Pyfhel()
he.contextGen(p=65537, m=2048*2, flagBatching=True, fracDigits=20)
print('keygen')
he.keyGen()
he.relinKeyGen(50, 2)
he.rotateKeyGen(50)

print('calc starts')
# c1 = he.encryptFrac(1.2)
# c2 = he.encryptFrac(2.3)
# c3 = c1 + c2*c2
# print(he.decryptFrac(c3))

# p1 = he.encodeFrac(2.0)
# c4 = c1 * p1
# print(he.decryptFrac(c4))

show_size = 10
c1 = he.encryptBatch([1,2,3])
print(he.decryptBatch(c1)[:show_size])

c2 = he.encryptBatch([3,4,5])
print(he.decryptBatch(c2)[:show_size])

## want to calculate inner product
c_mult = c1*c2
he.relinearize(c_mult)
print(he.decryptBatch(c_mult)[:show_size])


rot1 = he.rotate(c_mult, 1,  in_new_ctxt=True)
rot2 = he.rotate(c_mult, 2, in_new_ctxt=True)
print(he.decryptBatch(rot1)[:show_size])
print(he.decryptBatch(rot2)[:show_size])

c_add = c_mult + rot1 + rot2
print(he.decryptBatch(c_add)[:show_size])

p1 = he.encodeBatch([1,0,0])

result = he.multiply_plain(c_add, p1)
print(he.decryptBatch(result)[:show_size])

