from phe import paillier as pl

pk, sk = pl.generate_paillier_keypair(n_length=256)
c1 = pk.encrypt(1.2)
c2 = pk.encrypt(-3.4)

c3 = c1 + c2
c4 = c1 - c2

print(sk.decrypt(c3))
print(sk.decrypt(c4))

c5 = c1 * 2
c6 = c1 / 2

print(sk.decrypt(c5))
print(sk.decrypt(c6))


print('**'*20)
a1 = [1,2,3]
a2 = [3,4,5]
ca1 = [pk.encrypt(a1[i]) for i in range(len(a1))]
ca2 = [pk.encrypt(a2[i]) for i in range(len(a2))]

## want to calculate inner product of a1 and a2
ip = [ca1[i]*a2[i] for i in range(len(a1))]

tmp = ip[0]
for i in range(1, len(a1)):
    tmp += ip[i]

print('inner product result')
import numpy as np
print(np.dot(a1,a2))
print(sk.decrypt(tmp))

# dec_ip = [sk.decrypt(ip[i]) for i in range(len(ip))]
# print(dec_ip)

